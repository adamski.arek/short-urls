package com.example.short_url.endpoint.main_page;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class UrlFormRequestTest {

    @Test
    @DisplayName("Should produce valid JSON string")
    void toJson() throws JSONException {
        String text = "myAddress.com";
        UrlFormRequest urlFormRequest = new UrlFormRequest();
        urlFormRequest.setUrlText(text);
        final String actual = urlFormRequest.toJson();

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("longUrl", text);
        jsonObject.put("shortUrl", "");
        final String expected = jsonObject.toString();

        Assertions.assertEquals(expected, actual);
    }
}
