package com.example.short_url.endpoint;

import com.example.short_url.endpoint.utils.UnsupportedUrlException;
import com.example.short_url.endpoint.utils.UrlUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UrlUtilsTest {

    @Test
    @DisplayName("Empty string")
    void hasProtocol2() {
        final String text = "";
        final UrlUtils urlUtils = new UrlUtils();
        Assertions.assertThrows(UnsupportedUrlException.class, () -> {
                    final String actual = urlUtils.hasProtocol(text);
                }
        );
    }

    @Test
    @DisplayName("null")
    void hasProtocol1() {
        final String text = null;
        final UrlUtils urlUtils = new UrlUtils();
        Assertions.assertThrows(UnsupportedUrlException.class, () -> {
            final String actual = urlUtils.hasProtocol(text);
        });
    }

    @Test
    @DisplayName("Valid address, without protocol")
    void hasProtocol3() {
        final String text = "www.google.com";
        final UrlUtils urlUtils = new UrlUtils();
        final String actual = urlUtils.hasProtocol(text);
        final String expected = "https://" + text;

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Valid address with https protocol")
    void hasProtocol4() {
        final String text = "https://www.google.com";
        final UrlUtils urlUtils = new UrlUtils();
        final String actual = urlUtils.hasProtocol(text);
        final String expected = text;

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Valid address with http protocol")
    void hasProtocol5() {
        final String text = "http://www.google.com";
        final UrlUtils urlUtils = new UrlUtils();
        final String actual = urlUtils.hasProtocol(text);
        final String expected = text;

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Valid address with ftps protocol")
    void hasProtocol6() {
        final String text = "ftps://www.google.com";
        final UrlUtils urlUtils = new UrlUtils();
        final String actual = urlUtils.hasProtocol(text);
        final String expected = text;

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Valid address with ftp protocol")
    void hasProtocol7() {
        final String text = "ftp://www.google.com";
        final UrlUtils urlUtils = new UrlUtils();
        final String actual = urlUtils.hasProtocol(text);
        final String expected = text;

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Valid address with file protocol")
    void hasProtocol8() {
        final String text = "file://www.google.com";
        final UrlUtils urlUtils = new UrlUtils();
        final String actual = urlUtils.hasProtocol(text);
        final String expected = text;

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Valid address with unknown protocol")
    void hasProtocol9() {
        final String text = "test://www.google.com";
        final UrlUtils urlUtils = new UrlUtils();
        final String actual = urlUtils.hasProtocol(text);
        final String expected = text;

        assertEquals(expected, actual);
    }

}
