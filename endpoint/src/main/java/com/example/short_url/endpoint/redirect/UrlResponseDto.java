package com.example.short_url.endpoint.redirect;

/**
 * DTO object used to get data of url_store service
 */
public class UrlResponseDto {
    private String longUrl;
    private String shortUrl;

    public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }
}
