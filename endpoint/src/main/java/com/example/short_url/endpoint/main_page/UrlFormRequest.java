package com.example.short_url.endpoint.main_page;

public class UrlFormRequest {
    private String urlText;

    /**
     * @param urlText String to be set as longUrl
     */
    public void setUrlText(String urlText) {
        this.urlText = urlText;
    }

    /**
     * @return JSON string with longUrl field. In the JSON is included a shortUrl field (set to empty String).
     */
    public String toJson() {
        return "{\"shortUrl\":\"\",\"longUrl\":\"" + urlText + "\"}";
    }
}
