package com.example.short_url.endpoint.redirect;

import com.example.short_url.endpoint.utils.UnsupportedUrlException;
import com.example.short_url.endpoint.utils.UrlUtils;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * This controller is responsible for redirection endpoint
 */
@RestController
public class RedirectController {
    private final UrlUtils urlUtils;
    private final WebClient.Builder builder;


    public RedirectController(UrlUtils urlUtils, @LoadBalanced WebClient.Builder builder) {
        this.urlUtils = urlUtils;
        this.builder = builder;
    }

    /**
     * Make a call to statistic service to inform it, that redirection request occurred.
     * Make the redirect.
     *
     * @param serverHttpResponse Autowired entity, which make the redirection
     * @param shortUrl address, from which we want to redirect.
     * @return serverHttpResponse with redirection request.
     */
    @GetMapping("/r/{shortUrl}")
    public Mono<Void> redirect(ServerHttpResponse serverHttpResponse, @PathVariable String shortUrl) {
        // TODO this redirect controller will return 500, if there is any problem with connection to the url_store service
        //  it should be decided if this is desired.
        return builder
                .build()
                .get()
                .uri("http://url-shortcut-url-store/api/" + shortUrl)
                .exchange()
                .flatMap(clientResponse1 -> clientResponse1.toEntity(UrlResponseDto.class))
                .flatMap(urlResponseResponseEntity -> {

                    final UrlResponseDto body = urlResponseResponseEntity.getBody();
                    if (body != null && body.getLongUrl() != null) {
                        final String statValue = String.format("{\"shortUrl\":\"%s\",\"longUrl\":\"%s\"}", shortUrl, body.getLongUrl());
                        System.out.println(statValue);
                        // Send information to statistics, that redirection occurred
                        builder
                                .build()
                                .post()
                                .uri("http://url-shortcut-statistics/")
                                .contentType(MediaType.APPLICATION_JSON)
                                .contentLength(statValue.length() * 2)
                                .bodyValue(statValue)
                                .retrieve()
                                .toBodilessEntity()
                                // We are not interesting in the answer, so just execute call
                                .subscribe();

                        String longUrl = body.getLongUrl();
                        longUrl = urlUtils.hasProtocol(longUrl);

                        // Redirect to new page
                        serverHttpResponse.setStatusCode(HttpStatus.TEMPORARY_REDIRECT);
                        serverHttpResponse.getHeaders().add("Location", longUrl);
                        return serverHttpResponse.setComplete();
                    }
                    // TODO This simple application has almost no gui, so there is no real error pages
                    //  If any error will occur, 500 and default error page will be shown
                    return Mono.error(new UnsupportedUrlException());
                });

    }

    @ExceptionHandler(UnsupportedUrlException.class)
    public ResponseEntity<String> handleWebClientResponseException(UnsupportedUrlException e) {

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Address not found.");
    }
}
