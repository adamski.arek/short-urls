package com.example.short_url.endpoint.main_page;

import com.example.short_url.endpoint.redirect.UrlResponseDto;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@SuppressWarnings("unused")
@Controller
public class MainPageController {

    private final WebClient.Builder builder;

    public MainPageController(@LoadBalanced WebClient.Builder builder) {
        this.builder = builder;
    }


    /**
     * Shows Form for enter Long Url
     *
     * @param model Autowired model
     * @return Mono with template name.
     */
    @GetMapping("/")
    Mono<String> getIndexPage(Model model) {

        model.addAttribute("urlExists", false)
                .addAttribute("urlShortcut", "")
                .addAttribute("errorExists", false);
        return Mono.just("index");
    }

    /**
     * Receives long Url strings, and sends request to UrlStore service.
     * UrlStore service sends back shortUrl
     * If all data are back, show them on the page.
     *
     * @param urlFormRequest Form data. It contains only long url String.
     * @param model Autowired model
     * @param request Autowired ServerHttpRequest
     * @return Mono containing template name.
     */
    @PostMapping("/")
    Mono<String> postIndexPage(UrlFormRequest urlFormRequest, Model model, ServerHttpRequest request) {
        // Prepare long url to be send to the store
        // TODO there should be here some string verification and sanitation like: check for protocol,
        //  form of the string, length. I put here no, because I do not know the requirements.
        final String bodyPayload = urlFormRequest.toJson();
        return builder
                .build()
                .post()
                .uri("http://url-shortcut-url-store/api/")
                .contentType(MediaType.APPLICATION_JSON)
                .contentLength(bodyPayload.length() * 2)
                .bodyValue(bodyPayload)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .toEntity(UrlResponseDto.class)
                .flatMap(urlResponseResponseEntity -> {
                    final UrlResponseDto body = urlResponseResponseEntity.getBody();
                    final boolean urlExists;
                    final String urlShortcut;
                    final boolean errorExists;

                    // Check if we got all the data back from store. If yes, show them on the page.
                    // If not, show error message
                    if (body != null && body.getShortUrl() != null && body.getLongUrl() != null) {
                        // Prepare short url containing redirect server address
                        urlExists = true;
                        urlShortcut = request.getURI().toString() + "r/" + body.getShortUrl();
                        errorExists = false;

                    } else {
                        // There is no data returned, show error
                        urlExists = false;
                        urlShortcut = "";
                        errorExists = true;
                    }
                    model.addAttribute("urlExists", urlExists);
                    model.addAttribute("urlShortcut", urlShortcut);
                    model.addAttribute("errorExists", errorExists);

                    return Mono.just("index");
                });
    }
}
