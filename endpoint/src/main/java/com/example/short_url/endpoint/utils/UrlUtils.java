package com.example.short_url.endpoint.utils;

import org.springframework.stereotype.Component;

@Component
public class UrlUtils {

    /**
     * Checks if string has a protocol prefix. If it has one, then the string is returned.
     * If there is no protocol, the https protocol is added.
     *
     * @param url string
     * @return url string with a protocol.
     */
    public String hasProtocol(String url) {
        if (url == null || url.length() == 0)
            throw new UnsupportedUrlException();

        final String result;
        // If we'd like to pass only http, ftp and file
        // ^(https?|ftps?|file)\:\/\/.*
        if (url.matches("^\\w+\\:\\/\\/.*")) {
            result = url;
        } else {
            result = "https://" + url;
        }

        return result;
    }
}
