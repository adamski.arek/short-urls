package com.example.short_url.url_store.utils.codecs;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.crypto.NoSuchPaddingException;
import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class EncryptCodecUtilsTest {

    @Test
    @DisplayName("Should encode")
    void encode() throws NoSuchAlgorithmException, NoSuchPaddingException {
        final String text = RandomStringUtils.randomAlphabetic(10, 20);
        final EncryptCodecUtils encryptCodecUtils = new EncryptCodecUtils();
        final String encode = encryptCodecUtils.encode(text);

        assertNotEquals(text, encode);
    }

    @Test
    @DisplayName("Should decode")
    void decode() throws NoSuchAlgorithmException, NoSuchPaddingException {
        // Some pre encoded value
        final String text = "s9qTjrfkbPANxNE7/bJDwfNpvaPHJHNBbTfII9qdUe8=";
        final EncryptCodecUtils encryptCodecUtils = new EncryptCodecUtils();
        final String decode = encryptCodecUtils.decode(text);

        assertNotEquals(text, decode);
    }

    @Test
    @DisplayName("Encode and decode should match")
    void encodeDecode() throws NoSuchAlgorithmException, NoSuchPaddingException {
        final String text = RandomStringUtils.randomAlphabetic(15, 30);
        final EncryptCodecUtils encryptCodecUtils = new EncryptCodecUtils();
        final String encode = encryptCodecUtils.encode(text);
        System.out.println("Encode" + encode);
        final String actual = encryptCodecUtils.decode(encode);

        assertEquals(text, actual);
    }
}
