package com.example.short_url.url_store.utils.codecs;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class SimpleCodecUtilsTest {

    @Test
    @DisplayName("Encryption should return value which was not passed")
    void test1 () {
        final SimpleCodecUtils simpleCodecUtils = new SimpleCodecUtils();
        final String string = RandomStringUtils.randomAlphabetic(10, 16);
        final String actual = simpleCodecUtils.encode(string);

        assertNotEquals(string, actual);
    }

    @Test
    @DisplayName("Decryption should return value which was not passed")
    void test2 () {
        final SimpleCodecUtils simpleCodecUtils = new SimpleCodecUtils();
        final String string = "NtcwKXNlcLUi";
        final String actual = simpleCodecUtils.decode(string);

        assertNotEquals(string, actual);
    }

    @Test
    @DisplayName("What was encoded, should be decoded")
    void test3 () {
        final SimpleCodecUtils simpleCodecUtils = new SimpleCodecUtils();
        final String string = RandomStringUtils.randomAlphabetic(10, 16);
        final String encoded = simpleCodecUtils.encode(string);

        final String actual = simpleCodecUtils.decode(encoded);

        assertEquals(string, actual);
    }
}
