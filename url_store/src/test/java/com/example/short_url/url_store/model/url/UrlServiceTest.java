package com.example.short_url.url_store.model.url;

import com.example.short_url.url_store.utils.codecs.CodecUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@ExtendWith(MockitoExtension.class)
class UrlServiceTest {
    @Mock
    UrlRepository urlRepository;
    @Mock
    CodecUtils codecUtils;

    @Test
    @DisplayName("Check if all methods are called with save")
    void save() {
        final UrlService urlService = new UrlService(codecUtils, urlRepository);
        final UrlEntity urlEntity = new UrlEntity();
        urlEntity.setId(new Random().nextLong());
        urlEntity.setShortUrl(RandomStringUtils.randomAlphabetic(10,19));
        urlEntity.setLongUrl(RandomStringUtils.randomAlphabetic(30,40));
        urlService.save(urlEntity);

        Mockito.verify(codecUtils, Mockito.times(1)).encode(urlEntity.getId().toString());
        Mockito.verify(urlRepository, Mockito.times(1)).save(urlEntity);
    }

    @Test
    @DisplayName("Returns existing object")
    void getByShortUrl1() {
        final UrlService urlService = new UrlService(codecUtils, urlRepository);
        final UrlEntity urlEntity = new UrlEntity();
        final Long id = new Random().nextLong();
        urlEntity.setId(id);
        final String shortUrl = RandomStringUtils.randomAlphabetic(10, 19);
        urlEntity.setShortUrl(shortUrl);
        urlEntity.setLongUrl(RandomStringUtils.randomAlphabetic(30,40));

        Mockito.when(codecUtils.decode(shortUrl)).thenReturn(id.toString());

        Mockito.when(urlRepository.findById(id)).thenReturn(Optional.of(urlEntity));

        final Optional<UrlEntity> actualUrl = urlService.getByShortUrl(shortUrl);

        assertEquals(urlEntity, actualUrl.get());
    }

    @Test
    @DisplayName("Returns empty optional")
    void getByShortUrl2() {
        final UrlService urlService = new UrlService(codecUtils, urlRepository);
        final UrlEntity urlEntity = new UrlEntity();
        final Long id = new Random().nextLong();
        urlEntity.setId(id);
        final String shortUrl = RandomStringUtils.randomAlphabetic(10, 19);
        urlEntity.setShortUrl(shortUrl);
        urlEntity.setLongUrl(RandomStringUtils.randomAlphabetic(30,40));

        Mockito.when(codecUtils.decode(shortUrl)).thenReturn(id.toString());

        Mockito.when(urlRepository.findById(id)).thenReturn(Optional.empty());

        final Optional<UrlEntity> actualUrl = urlService.getByShortUrl(shortUrl);

        assertFalse(actualUrl.isPresent());
    }
}
