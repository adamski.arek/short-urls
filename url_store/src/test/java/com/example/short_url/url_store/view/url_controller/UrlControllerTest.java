package com.example.short_url.url_store.view.url_controller;

import com.example.short_url.test_utils.JsonFactory;
import com.example.short_url.url_store.model.url.UrlEntity;
import com.example.short_url.url_store.model.url.UrlService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.hamcrest.Matchers.containsString;

@SpringBootTest
@AutoConfigureMockMvc
class UrlControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    UrlService urlService;

    @Test
    @DisplayName("Get request")
    void getApi() throws Exception {
        final UrlEntity urlEntity = new UrlEntity();
        urlEntity.setLongUrl("www.google.com");
        urlEntity.setShortUrl("wweess");
        Mockito
                .when(urlService.getByShortUrl(urlEntity.getShortUrl()))
                .thenReturn(Optional.of(urlEntity));

        final UrlResponseDto urlResponseDto = new UrlResponseDto()
                .setLongUrl(urlEntity.getLongUrl())
                .setShortUrl(urlEntity.getShortUrl());
        final String result = JsonFactory.fromUrlResponse(urlResponseDto);


        this.mockMvc
                .perform(MockMvcRequestBuilders.get("/api/" + urlEntity.getShortUrl()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(containsString(result)));
    }

    @Test
    @DisplayName("Post request")
    void postApi() throws Exception {
        final UrlEntity urlEntity = new UrlEntity();
        urlEntity.setLongUrl("www.yahoo.com");

        Mockito
                .doAnswer(invocation -> {
                    UrlEntity urlEnt = (UrlEntity) invocation.getArguments()[0];
                    urlEnt.setShortUrl("wsdsd");
                    return urlEnt;
                })
                .when(urlService)
                .save(urlEntity);

        UrlResponseDto urlResponseDto = new UrlResponseDto()
                .setLongUrl(urlEntity.getLongUrl())
                .setShortUrl("wsdsd");

        final String result = JsonFactory.fromUrlResponse(urlResponseDto);

        final String content = JsonFactory.fromLongUrlString(urlEntity.getLongUrl());

        this.mockMvc
                .perform(MockMvcRequestBuilders.post("/api/").contentType(MediaType.APPLICATION_JSON).content(content))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().string(containsString(result)));
    }
}
