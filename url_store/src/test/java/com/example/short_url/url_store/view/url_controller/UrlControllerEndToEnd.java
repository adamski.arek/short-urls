package com.example.short_url.url_store.view.url_controller;

import com.example.short_url.test_utils.JsonFactory;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class UrlControllerEndToEnd {

    @Autowired
    MockMvc mockMvc;

    @Test
    @DisplayName("Add and retrieve address from store should give you same address")
    void end2EndTest() throws Exception {
        final String content = JsonFactory.fromLongUrlString("www.google.com");
        final MvcResult mvcResult = this.mockMvc
                .perform(MockMvcRequestBuilders.post("/api/").contentType(MediaType.APPLICATION_JSON).content(content))
                .andReturn();
        final MockHttpServletResponse response = mvcResult.getResponse();
        final String contentAsString = response.getContentAsString();

        final JSONObject jsonObject = new JSONObject(contentAsString);
        final Object shortUrl = jsonObject.get("shortUrl");

        this.mockMvc
                .perform(MockMvcRequestBuilders.get("/api/" + shortUrl))
                .andExpect(MockMvcResultMatchers.content().string("{\"longUrl\":\"www.google.com\",\"shortUrl\":null}"));
    }
}
