package com.example.short_url.test_utils;

import com.example.short_url.url_store.view.url_controller.UrlResponseDto;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public abstract class JsonFactory {

    public static String fromUrlResponse(UrlResponseDto urlResponseDto) {
        final com.fasterxml.jackson.core.JsonFactory jsonFactory = new com.fasterxml.jackson.core.JsonFactory();
        final OutputStream os = new ByteArrayOutputStream();

        try (JsonGenerator generator = jsonFactory.createGenerator(os)) {
            generator.writeStartObject();
            generator.writeStringField("longUrl", urlResponseDto.getLongUrl());
            generator.writeStringField("shortUrl", urlResponseDto.getShortUrl());
            generator.writeEndObject();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return os.toString();
    }

    public static String fromLongUrlString(String longUrl) {
        final com.fasterxml.jackson.core.JsonFactory jsonFactory = new com.fasterxml.jackson.core.JsonFactory();
        final OutputStream os = new ByteArrayOutputStream();

        try (JsonGenerator generator = jsonFactory.createGenerator(os)) {
            generator.writeStartObject();
            generator.writeStringField("longUrl", longUrl);
            generator.writeEndObject();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return os.toString();
    }
}
