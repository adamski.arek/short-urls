package com.example.short_url.url_store.model.url;

import com.example.short_url.url_store.utils.codecs.CodecUtils;
import com.example.short_url.url_store.utils.exceptions.CodingException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UrlService {
    private final CodecUtils codecUtils;
    private final UrlRepository urlRepository;

    public UrlService(CodecUtils codecUtils, UrlRepository urlRepository) {
        this.codecUtils = codecUtils;
        this.urlRepository = urlRepository;
    }

    /**
     * Saves urlEntity to database, and calculates shortUrl on an id generated by the database.
     * @param urlEntity to be saved
     */
    public void save(UrlEntity urlEntity) {
        urlRepository.save(urlEntity);
        final Long id = urlEntity.getId();
        final String shortUrl = codecUtils.encode(id.toString());
        urlEntity.setShortUrl(shortUrl);
    }

    /**
     * Returns optional of url. This method does not set shortUrl on UrlEntity object.
     *
     * @param shortUrl shortUrl to be parsed and used as an ID to get data from database
     * @return Optional of UrlEntity
     *
     * @throws NumberFormatException  if the string does not contain a parsable shortUrl.
     * @throws CodingException  if the string does not contain a parsable shortUrl.
     */
    public Optional<UrlEntity> getByShortUrl(String shortUrl) {
        final String decode = codecUtils.decode(shortUrl);
        final Long id = Long.parseLong(decode);
        return urlRepository.findById(id);
    }
}
