package com.example.short_url.url_store;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UrlStoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(UrlStoreApplication.class, args);
    }

}
