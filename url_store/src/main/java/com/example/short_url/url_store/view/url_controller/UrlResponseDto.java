package com.example.short_url.url_store.view.url_controller;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UrlResponseDto {
    private String longUrl;
    private String shortUrl;
}
