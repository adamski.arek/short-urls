package com.example.short_url.url_store.view.url_controller;

import com.example.short_url.url_store.model.url.UrlEntity;
import com.example.short_url.url_store.model.url.UrlService;
import com.example.short_url.url_store.utils.exceptions.CodingException;
import com.example.short_url.url_store.utils.exceptions.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Objects;
import java.util.Optional;

@RestController
public class UrlController {
    private final UrlService urlService;

    public UrlController(UrlService urlService) {
        this.urlService = urlService;
    }

    @GetMapping("/api/{shortUrlString}")
    UrlResponseDto getUrl(@PathVariable String shortUrlString) {
        try {
            final Optional<UrlEntity> byShortUrl = urlService.getByShortUrl(shortUrlString);

            UrlEntity longUrl = byShortUrl.orElseThrow(EntityNotFoundException::new);

            return new UrlResponseDto()
                    .setShortUrl(longUrl.getShortUrl())
                    .setLongUrl(longUrl.getLongUrl());

        } catch (CodingException | NullPointerException | IllegalArgumentException | EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Entity not found.");
        }
    }

    @PostMapping(value = "/api/")
    @ResponseStatus(HttpStatus.CREATED)
    UrlResponseDto postUrl(@RequestBody UrlRequestDto urlRequestDto) {
        try {
            final String longUrl = urlRequestDto.getLongUrl();
            // TODO there should be more sophisticated check, then just check if is not null; ie length pattern,
            //  depending on requirements
            Objects.requireNonNull(longUrl, "Url cannot be null");

            UrlEntity urlEntity = new UrlEntity(longUrl);

            urlService.save(urlEntity);

            final String shortUrl = urlEntity.getShortUrl();

            return new UrlResponseDto()
                    .setLongUrl(longUrl)
                    .setShortUrl(shortUrl);

        } catch (CodingException | NullPointerException | IllegalArgumentException | EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Entity could not be saved.");
        }
    }

}
