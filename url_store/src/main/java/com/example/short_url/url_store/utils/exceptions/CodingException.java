package com.example.short_url.url_store.utils.exceptions;

public class CodingException extends RuntimeException {
    public CodingException(Throwable cause) {
        super(cause);
    }
}
