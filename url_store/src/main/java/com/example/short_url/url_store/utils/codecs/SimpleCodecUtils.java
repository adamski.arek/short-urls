package com.example.short_url.url_store.utils.codecs;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
@Primary
public class SimpleCodecUtils implements CodecUtils {
    /**
     * Encodes text using Base64
     *
     * @param text to be encoded
     * @return encoded string
     */
    @Override
    public String encode(String text) {
        final Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(text.getBytes());
    }

    /**
     * Decodes text encoded with Base64
     *
     * @param text to be decoded
     * @return decoded string
     * @throws  IllegalArgumentException if {@code text} is not in valid Base64 scheme
     */
    @Override
    public String decode(String text) {
        final Base64.Decoder decoder = Base64.getDecoder();
        final byte[] actual = decoder.decode(text);
        return new String(actual);
    }
}
