package com.example.short_url.url_store.view.url_controller;

import lombok.Data;

@Data
public class UrlRequestDto {
    private String shortUrl;
    private String longUrl;
}
