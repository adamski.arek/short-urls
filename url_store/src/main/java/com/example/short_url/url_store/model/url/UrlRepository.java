package com.example.short_url.url_store.model.url;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
interface UrlRepository extends CrudRepository<UrlEntity, Long> {

    Optional<UrlEntity> findById(Long id);
}
