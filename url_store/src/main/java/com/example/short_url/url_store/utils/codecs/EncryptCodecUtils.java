package com.example.short_url.url_store.utils.codecs;

import com.example.short_url.url_store.utils.exceptions.CodingException;
import org.springframework.stereotype.Component;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * This codec is not used in current implementation
 */
@Component
public class EncryptCodecUtils implements CodecUtils {

    private final SecretKey secretKey;
    private final Cipher cipher;
    private final Base64.Decoder decoder;
    private final Base64.Encoder encoder;

    public EncryptCodecUtils() throws NoSuchPaddingException, NoSuchAlgorithmException {
        final String key = "1234567812345678";
        secretKey = new SecretKeySpec(key.getBytes(), "AES");
        cipher = Cipher.getInstance("AES");
        decoder = Base64.getDecoder();
        encoder = Base64.getEncoder();
    }

    /**
     * Encodes text using AES codec. The result is encoded with Base64.
     *
     * @param text to be encoded
     * @return encoded string
     * @throws CodingException if this cipher is in a wrong state
     *                         (e.g., has not been initialized);
     *                         if this cipher is a block cipher,
     *                         no padding has been requested (only in encryption mode), and the total
     *                         input length of the data processed by this cipher is not a multiple of
     *                         block size; or if this encryption algorithm is unable to
     *                         process the input data provided.
     *                         if this cipher is in decryption mode,
     *                         and (un)padding has been requested, but the decrypted data is not
     *                         bounded by the appropriate padding bytes
     *                         if this cipher is decrypting in an
     *                         AEAD mode (such as GCM/CCM), and the received authentication tag
     *                         does not match the calculated value
     */
    public String encode(String text) {
        try {
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            final byte[] result = cipher.doFinal(text.getBytes(StandardCharsets.UTF_8));
            return encoder.encodeToString(result);
        } catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            throw new CodingException(e);
        }
    }

    /**
     * Decodes text encoded with Base64 and AES codec
     *
     * @param text String to be decoded
     * @return decoded string
     * @throws CodingException if {@code src} is not in valid Base64 scheme
     *                         if this cipher is in a wrong state
     *                         (e.g., has not been initialized)
     *                         if this cipher is a block cipher,
     *                         no padding has been requested (only in encryption mode), and the total
     *                         input length of the data processed by this cipher is not a multiple of
     *                         block size; or if this encryption algorithm is unable to
     *                         process the input data provided.
     *                         if this cipher is in decryption mode,
     *                         and (un)padding has been requested, but the decrypted data is not
     *                         bounded by the appropriate padding bytes
     *                         if this cipher is decrypting in an
     *                         AEAD mode (such as GCM/CCM), and the received authentication tag
     *                         does not match the calculated value
     */
    public String decode(String text) {
        try {
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            final byte[] bytes = decoder.decode(text);
            final byte[] result = cipher.doFinal(bytes);
            return new String(result);
        } catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            throw new CodingException(e);
        }
    }
}
