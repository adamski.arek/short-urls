package com.example.short_url.url_store.utils.codecs;

public interface CodecUtils {

    String encode(String text);

    String decode(String text);
}
