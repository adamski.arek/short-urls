package com.example.short_url.url_store.model.url;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "urls")
public class UrlEntity {
    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "long_url")
    private String longUrl;
    @Transient
    private String shortUrl;

    public UrlEntity() {
    }

    public UrlEntity(String longUrl) {
        this.longUrl = longUrl;
    }
}
