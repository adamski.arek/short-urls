package com.example.short_url.statistics.model.log;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
interface RedirectLogRepository extends CrudRepository<RedirectLog, Long> {

    Optional<RedirectLog> findByShortUrlAndLongUrlAndYearAndMonth(String shortUrl, String longUrl, int year, int month);
}
