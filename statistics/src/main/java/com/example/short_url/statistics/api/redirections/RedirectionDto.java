package com.example.short_url.statistics.api.redirections;

import lombok.Data;

@Data
public class RedirectionDto {
    private String shortUrl;
    private String longUrl;
}
