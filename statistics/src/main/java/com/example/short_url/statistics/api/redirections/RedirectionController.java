package com.example.short_url.statistics.api.redirections;

import com.example.short_url.statistics.model.log.RedirectLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller responsible for gathering of usage statistic
 */
@RestController
public class RedirectionController {
    private final RedirectLogService redirectLogService;

    @Autowired
    public RedirectionController(RedirectLogService redirectLogService) {
        this.redirectLogService = redirectLogService;
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    void postRedirection(@RequestBody RedirectionDto redirectionDto) {
        redirectLogService.increment(redirectionDto.getShortUrl(), redirectionDto.getLongUrl());
    }
}
