package com.example.short_url.statistics.model.log;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Accessors(chain = true)
@Entity
public class RedirectLog {
    @Id
    @GeneratedValue
    private long Id;

    private String longUrl;
    private String shortUrl;
    private int year;
    private int month;
    private long quantity;
}
