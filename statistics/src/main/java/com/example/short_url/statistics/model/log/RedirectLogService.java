package com.example.short_url.statistics.model.log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class RedirectLogService {
    private final RedirectLogRepository redirectLogRepository;

    @Autowired
    public RedirectLogService(RedirectLogRepository redirectLogRepository) {
        this.redirectLogRepository = redirectLogRepository;
    }

    /**
     * Checks if there is a record in database for a current month and short and long urls.
     * If record exists, it increments its quantity.
     * If not, it creates a record in databases with quantity set to 1
     *
     * @param shortUrl shortUrl string to be saved in database
     * @param longUrl longUrl string to be saved in database
     * @return RedirectLog entity (saved in the database)
     */
    public RedirectLog increment(String shortUrl, String longUrl) {
        final int year = LocalDate.now().getYear();
        final int monthValue = LocalDate.now().getMonthValue();
        return incrementWithYearMonth(shortUrl, longUrl, year, monthValue);
    }

    /**
     * This method is extracted from increment() for testing only. That way we can easily mockup dates. It will be inlined anyway.
     *
     * @param shortUrl shortUrl string to be saved in database
     * @param longUrl longUrl string to be saved in database
     * @param year int for the year
     * @param monthValue int for the month. 1 is January
     *
     * @return RedirectLog entity (saved in the database)
     */
    RedirectLog incrementWithYearMonth(String shortUrl, String longUrl, int year, int monthValue) {
        // Check if there is any entry for this short, long, month and year
        final Optional<RedirectLog> optionalRedirectLog = redirectLogRepository.findByShortUrlAndLongUrlAndYearAndMonth(shortUrl, longUrl, year, monthValue);
        final RedirectLog redirectLog;
        if (optionalRedirectLog.isPresent()) {
            // If entity is present, just increment quantity
            redirectLog = optionalRedirectLog.get();
            redirectLog.setQuantity(redirectLog.getQuantity() + 1);
        } else {
            // There is no such entity, create new one
            redirectLog = new RedirectLog();
            redirectLog.setShortUrl(shortUrl);
            redirectLog.setLongUrl(longUrl);
            redirectLog.setMonth(monthValue);
            redirectLog.setYear(year);
            redirectLog.setQuantity(1);
        }
        redirectLogRepository.save(redirectLog);
        return redirectLog;
    }
}
