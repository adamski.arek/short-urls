package com.example.short_url.statistics.model.log;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class RedirectLogServiceTest {
    @Mock
    private RedirectLogRepository repository;

    @Test
    @DisplayName("When data returns in a database the counter should be incremented")
    void incrementWithYearMonth1() {

        final RedirectLogService redirectLogService = new RedirectLogService(repository);
        final String shortUrl = RandomStringUtils.randomAlphabetic(5, 10);
        final String longUrl = RandomStringUtils.randomAlphabetic(25, 45);
        final Random random = new Random();
        final int year = random.nextInt(11) + 2000;
        final int month = random.nextInt(11) + 1;

        final RedirectLog redirectLog = new RedirectLog()
                .setId(1)
                .setShortUrl(shortUrl)
                .setLongUrl(longUrl)
                .setQuantity(1)
                .setYear(year)
                .setMonth(month);


        Mockito
                .when(repository.findByShortUrlAndLongUrlAndYearAndMonth(shortUrl, longUrl, year, month))
                .thenReturn(Optional.of(redirectLog));

        final RedirectLog actual = redirectLogService.incrementWithYearMonth(shortUrl, longUrl, year, month);

        redirectLog.setQuantity(redirectLog.getQuantity()+1);

        assertEquals(redirectLog, actual);
    }

    @Test
    @DisplayName("When there is no such data in a database, counter should be set to 1")
    void incrementWithYearMonth2() {

        final RedirectLogService redirectLogService = new RedirectLogService(repository);
        final String shortUrl = RandomStringUtils.randomAlphabetic(5, 10);
        final String longUrl = RandomStringUtils.randomAlphabetic(25, 45);
        final Random random = new Random();
        final int year = random.nextInt(11) + 2000;
        final int month = random.nextInt(11) + 1;

        final RedirectLog redirectLog = new RedirectLog()
                .setShortUrl(shortUrl)
                .setLongUrl(longUrl)
                .setQuantity(1)
                .setYear(year)
                .setMonth(month);


        Mockito
                .when(repository.findByShortUrlAndLongUrlAndYearAndMonth(shortUrl, longUrl, year, month))
                .thenReturn(Optional.empty());

        final RedirectLog actual = redirectLogService.incrementWithYearMonth(shortUrl, longUrl, year, month);

        assertEquals(redirectLog, actual);
    }
}
