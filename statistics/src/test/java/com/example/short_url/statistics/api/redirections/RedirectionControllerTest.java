package com.example.short_url.statistics.api.redirections;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureMockMvc
class RedirectionControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void postRedirection() throws Exception {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("shortUrl", RandomStringUtils.randomAlphabetic(5,10));
        jsonObject.put("longUrl", RandomStringUtils.randomAlphabetic(15,25));
        final String jsonString = jsonObject.toString();

        final MvcResult mvcResult = this.mockMvc
                .perform(MockMvcRequestBuilders.post("/").contentType(MediaType.APPLICATION_JSON).content(jsonString))
                .andReturn();

        final int actualStatus = mvcResult.getResponse().getStatus();

        assertEquals(HttpStatus.CREATED.value(), actualStatus);
    }
}
